import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type HNPostDocument = HNPost & Document;

@Schema()
export class HNPost {

	@Prop()
	title?: string;

	@Prop()
	author: string;

	@Prop()
	objectID: string;

	@Prop()
	_tags?: string[];

	@Prop()
	points?: number;

	@Prop()
	story_text?: string;

	@Prop()
	url?: string;

	@Prop()
	created_at_i?: Number;

	@Prop()
	comment_text?: string;

	@Prop()
	num_comments?: number;

	@Prop()
	story_id?: string;

	@Prop()
	story_title?: string;

	@Prop()
	story_url?: string;

	@Prop()
	parent_id?: string;

	@Prop()
	deleted?: boolean;
}

export const HNPostSchema = SchemaFactory.createForClass(HNPost);

HNPostSchema.index({objectID:1, author:1, story_id:1}, {unique:true});