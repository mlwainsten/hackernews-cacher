import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

const fs = require('fs');

async function bootstrap() {

	const app = await NestFactory.create(AppModule);

	const config = new DocumentBuilder()
	.setTitle('Hacker News Cacher')
	.setDescription("HN NodeJS API Documentation")
	.setVersion('1.0')
	.build();

	const document = SwaggerModule.createDocument(app, config);
	SwaggerModule.setup('api', app, document);
	// instantiate fs to write to disk


	fs.writeFileSync("./swagger-spec.json", JSON.stringify(document));

	await app.listen(3000);
}
bootstrap();
