import { Controller, Delete, Get, Param, Query, Res } from '@nestjs/common';
import { ApiQuery, ApiResponse } from '@nestjs/swagger';
import { query } from 'express';
import { title } from 'process';
import { AppService } from './app.service';
import { HNPost, HNPostDocument } from './schemas/HNPost.schema';

/*
The server, once an hour, should connect to the API (refer to the below url) which shows
recently posted articles about Node.js on Hacker News. It should insert the data from the
API into a database and also define a REST API which the client (e.g. Postman) will be used
to retrieve the data.
Hacker News URL: https://hn.algolia.com/api/v1/search_by_date?query=nodejs
The service should return paginated results with a maximum of 5 items and should be able
to be filtered by author, _tags, title. Also, this should permit the user to remove items and
these ones should not reappear when the app is restarted.
*/

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @ApiQuery({ name: 'author', type: String, required: false, description: 'Filter by author' })
  @ApiQuery({ name: 'tags', type: String, required: false, description: 'Filter by tags' })
  @ApiQuery({ name: 'title', type: String, required: false, description: 'Filter by title' })
  @ApiQuery({ name: 'page', type: Number, required: false, description: 'Page number' })

  @Get('/posts')
  async getPosts(@Query('author') author, @Query('tags') tags="", @Query('title') title, @Query('page') page): Promise<any>{
	let posts = await this.appService.getPosts(author, tags.split(','), title, page);
	let postCount = await this.appService.postCount()
	
	return {posts, nbPosts:postCount, page, nbPages:Math.ceil(postCount/5)};
  }


  @ApiResponse({ status: 201, description: 'The post has been successfully created.' })
  //api response for failed delete
  @ApiResponse({ status: 400, description: 'The post has not been successfully deleted.' })
  @Delete('/posts/:id')
  async deletePost(@Param('id') id, @Res() res) {
	const result = await this.appService.deletePost(id);
	if(result)
	{
		// send 201 status code
		res.status(201).send();
	}
	else
	{
		// send 400 status code
		res.status(400).send();
	}
  }
}
