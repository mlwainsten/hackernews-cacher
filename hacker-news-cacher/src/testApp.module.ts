import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { PostPollerService } from './postPoller.service';
import { HttpModule } from '@nestjs/axios';
import { HNPostSchema } from './schemas/HNPost.schema';

@Module({
  imports: [MongooseModule.forRoot('mongodb+srv://hackerNewsTester:XGNUPTywrRfPu3Wu@mongodbcluster.wwxlb.gcp.mongodb.net/hackerNewsTest?retryWrites=true&w=majority',{keepAlive:true}), MongooseModule.forFeature([{name:'HNPost', schema: HNPostSchema}]),HttpModule],
  controllers: [AppController],
  providers: [AppService],
})
export class TestAppModule {}
