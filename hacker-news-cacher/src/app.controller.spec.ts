import { HttpModule } from '@nestjs/axios';
import { getModelToken, MongooseModule, MongooseModuleOptions} from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HNPost, HNPostSchema } from './schemas/HNPost.schema';


import { MongoMemoryServer } from 'mongodb-memory-server';
import { Mongoose } from 'mongoose';

let mongod: MongoMemoryServer;

export const rootMongooseTestModule = (options: MongooseModuleOptions = {}) => MongooseModule.forRootAsync({
  useFactory: async () => {
    mongod = await  MongoMemoryServer.create();
    const mongoUri = await mongod.getUri();
    return {
      uri: mongoUri,
      ...options,
    }
  },
});

export const closeInMongodConnection = async () => {
  if (mongod) await mongod.stop();
}


describe('AppController', () => {
  let appController: AppController;

  //beforeEach(async () => {
  //  const app: TestingModule = await Test.createTestingModule({
  //    controllers: [AppController],
  //    imports: [AppService, MongooseModule.forRoot('mongodb+srv://hackerNewsTester:XGNUPTywrRfPu3Wu@mongodbcluster.wwxlb.gcp.mongodb.net/hackerNewsTest?retryWrites=true&w=majority',{keepAlive:true}), MongooseModule.forFeature([{name:'HNPost', schema: HNPostSchema}]),HttpModule,
//	{ provide: 'HNPostModel', useValue: HNPostSchema }],
  //  }).compile();

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
		imports: [rootMongooseTestModule(), MongooseModule.forFeature([{name:'HNPost', schema: HNPostSchema}]),HttpModule],
		providers: [AppService, AppController],

    }).compile();
    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {

	it('get posts', () => {
		expect(1).toBe(1);
	});
  });

  afterAll(async () =>
  {
	await closeInMongodConnection();
  });
});
