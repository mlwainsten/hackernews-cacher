# HackerNews Cacher

A simple API that gets posts from hackernews, saves them to a DB and allows you to get posts / delete them from the cache

# Configuration:

1. Clone
2. place provided .env file in base path (where src & test folders are placed) 
3. npm i
4. npm run start

Check out the [Swagger docs](http://localhost:/3000/api)!

