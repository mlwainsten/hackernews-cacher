import { Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose, { Model, Types } from 'mongoose';
import { HNPost, HNPostDocument } from './schemas/HNPost.schema';
/*
The server, once an hour, should connect to the API (refer to the below url) which shows
recently posted articles about Node.js on Hacker News. It should insert the data from the
API into a database and also define a REST API which the client (e.g. Postman) will be used
to retrieve the data.
Hacker News URL: https://hn.algolia.com/api/v1/search_by_date?query=nodejs
The service should return paginated results with a maximum of 5 items and should be able
to be filtered by author, _tags, title. Also, this should permit the user to remove items and
these ones should not reappear when the app is restarted.
*/

@Injectable()
export class AppService {
	constructor(@InjectModel(HNPost.name) private HNPost:Model<HNPostDocument>) {}
  
	async getPosts(author?:string, tags?:string[], title?:string, page=0): Promise<HNPostDocument[]> {
		let query = {deleted:false};
		if(author) query['author'] = author;
		if(tags && (tags.length != 1 && tags[0]!='')) query['_tags'] = {$all:tags};
		if(title) query['title'] = {$regex:title};
		
		return await this.HNPost.find(query).sort({created_at:-1}).skip(page *5).limit(5).exec();
	}

	async postCount(): Promise<number> {
		return await this.HNPost.countDocuments({deleted:false}).exec();
	}

	async addPost(post:HNPost): Promise<Boolean> {
		try
		{
			await this.HNPost.create(post);
			return true;
		}
		catch(err)
		{
			return false;
		}
	}

	async addPosts(posts:HNPost[]): Promise<any> {

	
		//convert posts to documents
		let docs = posts.map(post =>
			{
				post.deleted = undefined;
				return post;
			});

		return await this.HNPost.bulkWrite(docs.map((doc) => {
			return {
			updateOne: {
				filter: {objectID: doc.objectID,},
				update: doc,
				upsert: true
				}
			}
		}) as any[]).then((r)=> 
		{
			console.log("Done adding posts: "+r.upsertedCount+" new posts added");
			return {ok:true};
		})
		.catch((err)=> 
		{
			//console.log("Error adding posts: "+err);
			return {ok:false};
		});
		//res = await this.HNPost.bulkSave(docs, {ordered:false});
		//console.log(res.getWriteErrorCount()+" errors");
	}

	async deletePost(id:string): Promise<boolean> {
		try
		{
			let res = await this.HNPost.findByIdAndUpdate(new Types.ObjectId(id), {$set:{deleted:true}}, {new:true}).exec();
			console.log(res);
			return true;
		}
		catch(err)
		{
			return false;
		}
	}
}
