import { HttpService } from "@nestjs/axios";
import { Inject, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { rmSync } from "fs";
import { Model } from "mongoose";
import { firstValueFrom } from "rxjs";
import { AppService } from "./app.service";
import { HNURL, TIMEOUT } from "./constants";
import { HNPost, HNPostDocument } from "./schemas/HNPost.schema";


/*
The server, once an hour, should connect to the API (refer to the below url) which shows
recently posted articles about Node.js on Hacker News. It should insert the data from the
API into a database and also define a REST API which the client (e.g. Postman) will be used
to retrieve the data.
Hacker News URL: https://hn.algolia.com/api/v1/search_by_date?query=nodejs
The service should return paginated results with a maximum of 5 items and should be able
to be filtered by author, _tags, title. Also, this should permit the user to remove items and
these ones should not reappear when the app is restarted.
*/



@Injectable()
export class PostPollerService {
	appService: AppService;

	Aclient: any;
	Aindex: any;
	constructor(@Inject(AppService) appService:AppService, @Inject(HttpService) private httpService:HttpService, @InjectModel(HNPost.name) private HNPostModel:Model<HNPostDocument>)
	{
		this.appService = appService;
		console.log("Polling service started");
		this.updatePosts(this);
		setInterval(this.updatePosts, 36000000,this);
	}

	async updatePosts(self:any)
	{
		console.log("Updating posts");
		let done = false;
		let res = "";

		const ret = new Promise(async (resolve)=>
		{
			while(!done)
			{
				let res = await self.getPosts()
				if(res.ok && res.ok==true) done = true;
			}
			resolve(true);
		});

		await ret.then( () => console.log("Done updating posts. Waiting 1 hour") );
	}

	async getPosts():Promise<any>
	{
		let timeout = null;

		let prom = new Promise(async (resolve,reject)=>{
			let sub = this.httpService.get(HNURL).subscribe(async (response) => {
				if(response.status >= 200 || response.status <300)
				{
					let postsAdded = await this.appService.addPosts(response.data.hits);
					if(postsAdded.ok)
					{
						resolve({ok:true});
					}
					else if (postsAdded.err)
					{
						reject({error:postsAdded.err});
					}
				}
				else
				{
					reject({error:response.status});
				}
			});
			timeout = setTimeout(()=>{sub.unsubscribe(); reject("timeout");}, TIMEOUT);
		});
		return prom.then((ret)=>{clearTimeout(timeout); return ret;}).catch((err)=>{return err;});
	}


}