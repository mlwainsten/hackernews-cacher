import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { PostPollerService } from './postPoller.service';
import { HttpModule } from '@nestjs/axios';
import { HNPostSchema } from './schemas/HNPost.schema';


import { ConfigModule, ConfigService } from '@nestjs/config';
//XGNUPTywrRfPu3Wu



@Module({
  imports: [ConfigModule.forRoot(),MongooseModule.forRootAsync({imports:[ConfigModule], inject:[ConfigService], useFactory: async (config: ConfigService) =>
({
	uri: config.get('MONGODB_URI'),
	keepAlive:true
})}), MongooseModule.forFeatureAsync([{name:'HNPost', useFactory: ()=> {return HNPostSchema}}]),HttpModule],
  controllers: [AppController],
  providers: [AppService, PostPollerService],
})
export class AppModule {}
