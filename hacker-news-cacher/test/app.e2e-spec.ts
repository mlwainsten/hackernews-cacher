import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, Inject } from '@nestjs/common';
import * as request from 'supertest';
import { TestAppModule } from 'src/testApp.module';
import { AppModule} from '../src/app.module';


/*
The server, once an hour, should connect to the API (refer to the below url) which shows
recently posted articles about Node.js on Hacker News. It should insert the data from the
API into a database and also define a REST API which the client (e.g. Postman) will be used
to retrieve the data.
Hacker News URL: https://hn.algolia.com/api/v1/search_by_date?query=nodejs
The service should return paginated results with a maximum of 5 items and should be able
to be filtered by author, _tags, title. Also, this should permit the user to remove items and
these ones should not reappear when the app is restarted.
*/

describe('End to end tests', () => {
	let app: INestApplication;
	

	beforeEach(async () => {
	const moduleFixture: TestingModule = await Test.createTestingModule({
		imports: [AppModule],
	}).compile();

	app = moduleFixture.createNestApplication();
	await app.init();
	});

	afterAll(async () => {
	
		await app.close();
		
	});

	it('/posts (GET)', () => {
	request(app.getHttpServer()).get('/posts')
	.expect(200)
	.expect('Content-Type', 'application/json; charset=utf-8')
	.expect((res) =>
		{
			const body = res.body;
			if(!body.posts) throw new Error('No posts found');
			if(!(res.body.posts instanceof Array)) throw new Error('Body is not an array');
			if(body.posts.length > 5) throw new Error('Body has more than 5 items');
		})
	.end((err, res) => {
		if (err) throw err;
	});
	});

	it('/posts (GET with filters)', () => {
	request(app.getHttpServer()).get('/posts?author=cultofmetatron')
	.expect(200)
	.expect('Content-Type', 'application/json; charset=utf-8')
	.expect((res) =>
		{
			const body = res.body;
			if(!body.posts) throw new Error('No posts found');
			if(!(res.body.posts instanceof Array)) throw new Error('Body is not an array');
			if(body.posts.length > 5) throw new Error('Body has more than 5 items');
			if(body.posts.length < 1) throw new Error('Body has less than 1 item');

			//check for all posts if the author is cultofmetatron
			body.posts.forEach(post => {

				if(post.author !== 'cultofmetatron') throw new Error('Author is not cultofmetatron');
			});

			
		})
	.end((err, res) => {
		if (err) throw err;
	});
	});

	//test with tag filter ("comment"). Iterate through the posts and check if the tag "comment" is present in all of them.
	it('/posts (GET with tag filter)', () =>
	{
		request(app.getHttpServer()).get('/posts?tag=comment')
		.expect(200)
		.expect('Content-Type', 'application/json; charset=utf-8')
		.expect((res) =>
			{

				const body = res.body
				if(!body.posts) throw new Error('No posts found');
				if(!(res.body.posts instanceof Array)) throw new Error('Body is not an array');
				if(body.posts.length > 5) throw new Error('Body has more than 5 items');
				if(body.posts.length < 1) throw new Error('Body has less than 1 item');

				for(let i = 0; i < body.posts.length; i++)
				{

					if(!body.posts[i]._tags.includes('comment')) throw new Error('Tag is not comment');
					
				}
			}
		)
		.end((err, res) => {
			if (err) throw err;
		}
		);
	});

	//test with title filter ("node"). Iterate through the posts and check if the title of all of them has "node" in it.
	it('/posts (GET with title filter)', () =>
	{
		request(app.getHttpServer()).get('/posts?title=node')
		.expect(200)
		.expect('Content-Type', 'application/json; charset=utf-8')
		.expect((res) =>
			{
				const body = res.body;
				if(!body.posts) throw new Error('No posts found');
				if(!(res.body.posts instanceof Array)) throw new Error('Body is not an array');
				if(body.posts.length > 5) throw new Error('Body has more than 5 items');
				if(body.posts.length < 1) throw new Error('Body has less than 1 item');

				for(let i = 0; i < body.posts.length; i++)
				{
					if(!body.posts[i].title.includes('node')) throw new Error('Title is not node');
				}
			}
		)
		.end((err, res) => {
			if (err) throw err;
		}
		);
	});


});
